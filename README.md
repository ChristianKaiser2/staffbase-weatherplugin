Weather Plugin for Staffbase Employee App (Java)
====================

This plugin displays the temperature in Chemnitz from http://www.openweathermap.org/ (owm).

## Usage ##
* set pluginID ```export eyo_plugin_id=my.plugin.id```
* set pluginSecret ```export eyo_plugin_secret=abcde12345...```
* set owm_api_key  ```export owm_api_key=abcde12345...```
* build ```mvn clean package``` (generates war file in target folder)
* start ```mvn jetty:run```

## Overview ##

### OpenWeatherClient ###
Calls the owm API with location and unit system and returns the current temperature

### EyoSSO ###
Decrypts to payload (user name, locale, ...) provided by the eyo backend.
The payload is transferred via jwt. To make decryption work, a valid pluginID (```eyo_plugin_id```) and
pluginSecret (```eyo_plugin_secret```) must be set via java opts or as environmental variables.

### PluginModule ###
Basic Guice Module.

### PluginResource ###
Jersey API Endpoint receiving the sso call from eyo backend.

### ServletContextListener ###
Sets up guice injection for the current servlet context.

### index.jsp ###
View showing the username from the single sign on and the current temperature in Chemnitz.