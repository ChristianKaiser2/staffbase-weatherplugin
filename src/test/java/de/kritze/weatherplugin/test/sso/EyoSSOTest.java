package de.kritze.weatherplugin.test.sso;

import java.io.UnsupportedEncodingException;

import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.MalformedClaimException;
import org.jose4j.jwt.consumer.InvalidJwtException;
import org.jose4j.keys.HmacKey;
import org.jose4j.lang.InvalidKeyException;
import org.jose4j.lang.JoseException;
import org.jose4j.jwt.consumer.InvalidJwtSignatureException;
import org.junit.BeforeClass;
import org.junit.Test;

import com.google.inject.Guice;
import com.google.inject.Injector;

import de.kritze.weatherplugin.PluginModule;
import de.kritze.weatherplugin.sso.Data;
import de.kritze.weatherplugin.sso.EyoSSO;
import de.kritze.weatherplugin.test.testBehaviour.InvalidAudienceTestBehaviour;
import de.kritze.weatherplugin.test.testBehaviour.InvalidSecretTestBehaviour;
import de.kritze.weatherplugin.test.testBehaviour.NotMatchingSecretTestBehaviour;
import de.kritze.weatherplugin.test.testBehaviour.PositiveTestBehaviour;
import de.kritze.weatherplugin.test.testBehaviour.TestBehaviour;

/**
 * Tests for the class com.eyo.sample.EyoSSO.
 * Validates that the encryption and decryption works as desired.
 *
 */

public class EyoSSOTest {
		
	private static EyoSSO eyoSSO;
	
	@BeforeClass
	public static void setUp() {
		Injector injector = Guice.createInjector(new PluginModule());
		eyoSSO = injector.getInstance(EyoSSO.class);
		
		
	}
	
	/**
	 * Test for positive outcome. JWT and all values are correct.
	 * 
	 * @throws MalformedClaimException
	 * @throws InvalidJwtException
	 * @throws UnsupportedEncodingException
	 * @throws JoseException
	 */
	@Test
	public void testPositive() throws MalformedClaimException, InvalidJwtException, UnsupportedEncodingException, JoseException {
		test(new PositiveTestBehaviour());
	}
	
	/**
	 * Test for invalid audience set in the claim. Test is passed if an InvalidJwtException is thrown
	 * 
	 * @throws MalformedClaimException
	 * @throws InvalidJwtException
	 * @throws UnsupportedEncodingException
	 * @throws JoseException
	 */
	@Test(expected=InvalidJwtException.class)
	public void testInvalidAudience() throws UnsupportedEncodingException, MalformedClaimException, JoseException, InvalidJwtException {
		test(new InvalidAudienceTestBehaviour());
	}
	
	/**
	 * Test for invalid secret (modulo 8 on length is not 0). Test is passed if an InvalidKeyException is thrown
	 * 
	 * @throws MalformedClaimException
	 * @throws InvalidJwtException
	 * @throws UnsupportedEncodingException
	 * @throws JoseException
	 */
	@Test(expected=InvalidKeyException.class)
	public void testInvalidSecret() throws UnsupportedEncodingException, MalformedClaimException, JoseException, InvalidJwtException {
		test(new InvalidSecretTestBehaviour());
	}
	
	/**
	 * Test for not matching secrets on encryption and decryption Test is passed if an InvalidJwtException is thrown
	 * 
	 * @throws MalformedClaimException
	 * @throws InvalidJwtException
	 * @throws UnsupportedEncodingException
	 * @throws JoseException
	 */
	@Test(expected=InvalidJwtSignatureException.class)
	public void testNotMatchingSecret() throws UnsupportedEncodingException, MalformedClaimException, JoseException, InvalidJwtException {
		test(new NotMatchingSecretTestBehaviour());
	}
	
	/**
	 * Test run. Attempts to encrypt and then decrypt the values in the JwtClaim
	 * and checks if the result is the expected result defined by the TestBehaviour. 
	 * 
	 * @param testBehaviour	Defines the test target, sets needed Parameters and validates the test outcome
	 * @throws UnsupportedEncodingException
	 * @throws JoseException
	 * @throws MalformedClaimException
	 * @throws InvalidJwtException
	 */
	private void test(TestBehaviour testBehaviour) throws UnsupportedEncodingException, JoseException, MalformedClaimException, InvalidJwtException {
				//Encrypt the data with claims and HmacKEy
				HmacKey key = testBehaviour.getKey();
				JwtClaims claims = testBehaviour.getClaims();
				String encryptedPayload = testBehaviour.generateEncryptedPayload(claims,key);

				//Decrypt the data using the key
				Data decryptedData = eyoSSO.decrypt(encryptedPayload);
				
				//validate the data
				testBehaviour.validateDecryptedData(decryptedData, claims);
	}
	


}
