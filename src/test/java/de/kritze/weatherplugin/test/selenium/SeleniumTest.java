package de.kritze.weatherplugin.test.selenium;

import static org.junit.Assert.assertEquals;

import java.io.UnsupportedEncodingException;

import org.jose4j.jwt.JwtClaims;
import org.jose4j.keys.HmacKey;
import org.jose4j.lang.JoseException;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import de.kritze.weatherplugin.configuration.Configuration;
import de.kritze.weatherplugin.test.testBehaviour.InvalidAudienceTestBehaviour;
import de.kritze.weatherplugin.test.testBehaviour.NotMatchingSecretTestBehaviour;
import de.kritze.weatherplugin.test.testBehaviour.PositiveTestBehaviour;
import de.kritze.weatherplugin.test.testBehaviour.TestBehaviour;

/**
 * Selenium tests to validate if the authentication via single sign on works as expected in the browser.
 * 
 * At the moment it can only run on Firefox version < 47. Other WebDrivers (Chrome etc.) can be added via Maven. 
 * 
 */
public class SeleniumTest {
	
		//possible outcomes, read from the title tag of the site
		private static final String SUCCESS = "Weather Plugin";
		private static final String ERROR = "An error occurred";
			
	
		/**
		 * Tests if the SSO works with a valid JWT. User should be redirected to index.jsp
		 * 
		 * @throws UnsupportedEncodingException
		 * @throws JoseException
		 */
		@Test
		public void testPositive() throws UnsupportedEncodingException, JoseException {
			test(new PositiveTestBehaviour(),SUCCESS);
		}
		
		/**
		 * Tests if the SSO does not work if the audience is set wrong. User should be redirected to error.jsp
		 * 
		 * @throws UnsupportedEncodingException
		 * @throws JoseException
		 */
		@Test
		public void testInvalidAudience() throws UnsupportedEncodingException, JoseException {
			test(new InvalidAudienceTestBehaviour(),ERROR);
		}
				
		/**
		 * Test for not matching secrets on encryption and decryption. User should be redirected to error.jsp
		 * 
		 * @throws UnsupportedEncodingException
		 * @throws JoseException
		 */
		@Test
		public void testNotMatchingSecret() throws UnsupportedEncodingException, JoseException  {
			test(new NotMatchingSecretTestBehaviour(),ERROR);
		}
			
		/**
		 * Takes the desired TestBehaviour and the expected outcome as parameter 
		 * and starts Firefox (Version < 47) to test if the user gets redirected to index.jsp or error.jsp
		 * 
		 * Validation is happening via the page title because the url still points to the rest endpoint.
		 * 
		 * @param testBehaviour
		 * @param expectedOutcome
		 * @throws UnsupportedEncodingException
		 * @throws JoseException
		 */
		private void test(TestBehaviour testBehaviour, String expectedOutcome) throws UnsupportedEncodingException, JoseException {
			WebDriver driver = new FirefoxDriver();
			
			//Encrypt the data with claims and HmacKEy
			HmacKey key = testBehaviour.getKey();
			JwtClaims claims = testBehaviour.getClaims();
			String encryptedPayload = testBehaviour.generateEncryptedPayload(claims,key);
			
			//call the endpoint with the encrypted data
			driver.get("http://localhost:8080/staffbase-weatherplugin/api/"+Configuration.SSO_ENDPOINT_PATH+"?"+Configuration.SSO_ENDPOINT_PARAMETER+"="+encryptedPayload);
			assertEquals(expectedOutcome, driver.getTitle());
									
	        // Close the driver
	        driver.quit();
		}
	 
}
