package de.kritze.weatherplugin.test.weatherclient;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import de.kritze.weatherplugin.configuration.Configuration;
import de.kritze.weatherplugin.weatherclient.WeatherClient;
import de.kritze.weatherplugin.weatherclient.openweather.OpenWeatherClient;


public class WeatherClientTest {

	WeatherClient client;
	/**
	 * Instantiate the selenium webdriver
	 */
	@Before 
	public void setUp() {
		client = new OpenWeatherClient(Configuration.getInstance().getOpenweathermapAPIKey());
	}
	
	/**
	 * Test if a query with the location returns a result
	 */
	@Test
	public void testChemnitz() {
		assertNotNull(client.getTemperature("chemnitz"));	
		
		assertNotNull(client.getTemperature("chemnitz",WeatherClient.METRIC));	
	}
	
	/**
	 * Test if a query with the a nonexistent location returns null
	 */
	@Test
	public void testLocationNotFound() {	
		assertNull(client.getTemperature("NotARealLocation"));	
			
		assertNull(client.getTemperature("NotARealLocation",WeatherClient.METRIC));	
	}
	
	/**
	 * Test if the unit system is set correctly
	 */
	@Test
	public void testSameUnit() {
		double metric = client.getTemperature("chemnitz",WeatherClient.METRIC);
		double imperial = client.getTemperature("chemnitz",WeatherClient.IMPERIAL);
		
		//Calculate Celsius from Fahrenheit
		double imperialToMetric = 9/5 * imperial -32; 
		
		//Celsius is equal to the calculated value from fahrenheit +- 0.1 because doubles shouldn't be directly compared
		assertTrue((metric -0.1) < imperialToMetric && imperialToMetric < (metric + 0.1));	
	}
	
}
