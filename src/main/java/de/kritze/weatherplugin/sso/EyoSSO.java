package de.kritze.weatherplugin.sso;

import org.jose4j.jwa.AlgorithmConstraints;
import org.jose4j.jws.AlgorithmIdentifiers;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.MalformedClaimException;
import org.jose4j.jwt.consumer.InvalidJwtException;
import org.jose4j.jwt.consumer.JwtConsumer;
import org.jose4j.jwt.consumer.JwtConsumerBuilder;

import de.kritze.weatherplugin.configuration.Configuration;

public class EyoSSO {

	/**
	 * Decrypts/validates given eyo sso token and copies the values from the received token to a container for further use.
	 * 
	 * @param encryptedSSO the eyo sso token.
	 * @return A data container with the extracted values from the decrypted sso token.  
	 * @throws InvalidJwtException
	 * @throws MalformedClaimException
	 */
	public Data decrypt(final String encryptedSSO) throws InvalidJwtException, MalformedClaimException {

		final JwtConsumer c = new JwtConsumerBuilder()
				.setJwsAlgorithmConstraints(new
						AlgorithmConstraints(AlgorithmConstraints.ConstraintType.WHITELIST,
								AlgorithmIdentifiers.HMAC_SHA256))
				.setVerificationKey(Configuration.getInstance().getKey())
				.setExpectedAudience(true, Configuration.getInstance().getPluginId())
				.build();

		final JwtClaims jwtClaims = c.process(encryptedSSO).getJwtClaims();
		return new Data(
				jwtClaims.getClaimValue(Configuration.SSO_INSTANCE_ID, String.class),
				jwtClaims.getClaimValue(Configuration.SSO_USER_ID, String.class),
				jwtClaims.getClaimValue(Configuration.SSO_USER_EXTERNAL_ID, String.class),
				jwtClaims.getClaimValue(Configuration.SSO_USER_FIRSTNAME, String.class),
				jwtClaims.getClaimValue(Configuration.SSO_USER_LASTNAME, String.class),
				jwtClaims.getClaimValue(Configuration.SSO_USER_ROLE, String.class),
				jwtClaims.getClaimValue(Configuration.SSO_USER_LANGUAGE, String.class)
				);
	}
}
