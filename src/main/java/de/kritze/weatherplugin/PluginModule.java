package de.kritze.weatherplugin;

import com.google.inject.AbstractModule;

import de.kritze.weatherplugin.sso.EyoSSO;

public class PluginModule extends AbstractModule {

  @Override
  protected void configure() {
    bind(EyoSSO.class).asEagerSingleton();
  }  
}
