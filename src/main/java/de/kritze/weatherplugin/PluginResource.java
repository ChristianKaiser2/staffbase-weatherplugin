package de.kritze.weatherplugin;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jose4j.jwt.MalformedClaimException;
import org.jose4j.jwt.consumer.InvalidJwtException;

import com.google.inject.Inject;
import com.google.inject.Provider;

import de.kritze.weatherplugin.configuration.Configuration;
import de.kritze.weatherplugin.sso.Data;
import de.kritze.weatherplugin.sso.EyoSSO;
import de.kritze.weatherplugin.weatherclient.WeatherClient;
import de.kritze.weatherplugin.weatherclient.openweather.OpenWeatherClient;

/**
 * Defines the resource used for single sign on requests
 */
@Path("/")
public class PluginResource {

  private static final Logger logger = LogManager.getLogger(PluginResource.class);

  // Used for the decryption of the JWT
  @Inject
  private EyoSSO eyoSSO;
  
  @Inject
  protected Provider<HttpServletRequest> requestProvider;
  
  @Inject
  protected Provider<HttpServletResponse> responseProvider;
  
  /**
   * Endpoint for single sign on with a JSON web token.
   * <p>
   * Extracts the user data for single sign on from the JWT in the query parameter. 
   * If the validation of the JWT is successful, the user will be forwarded to 
   * index.jsp including the data from the SSO as attributes in the request (userId, username etc).
   * If the JWT is not valid, the user will be forwarded to error.jsp.
   * 
   * @param  httpRequest 	needed to forward to either index.jsp or error.jsp
   * @param  httpResponse 	needed to forward to either index.jsp or error.jsp
   * @param  context 		needed to forward to either index.jsp or error.jsp
   * @param  encryptedSSO 	the SSO-data in form of a JWT
   * @return null-response if forwarding to index.jsp or error.jsp, Internal server error responce if forwarding was not possible
   */
  @GET
  @Path(Configuration.SSO_ENDPOINT_PATH)
  public Response eyosso(
      @Context final ServletContext context,
      @QueryParam(Configuration.SSO_ENDPOINT_PARAMETER) final String encryptedSSO) {
	  
    if (logger.isDebugEnabled()) {
      logger.debug("sso started.  Received token: {}",encryptedSSO);
    }
    
    HttpServletRequest httpRequest = requestProvider.get(); 
    HttpServletResponse httpResponse = responseProvider.get(); 
        
    Data data;
    try {
      //decrypt the encrypted message and transfer the values to a data container
      data = eyoSSO.decrypt(encryptedSSO);
    } catch (InvalidJwtException e) {
      logger.error("Unable to decode sso via jwt: {}", e.getMessage(), e);
      return forwardError(httpRequest, httpResponse, context);
    } catch (MalformedClaimException e) {
      logger.error("Unable to read claimed jwt value: {}", e.getMessage(), e);
      return forwardError(httpRequest, httpResponse, context);
    }

    if (logger.isDebugEnabled()) {
      logger.debug("sso successful: {}", data);
    }

    //Add the values from the JWT to the request which will forward the user to index.jsp. The values are stored in the data object.
    final RequestDispatcher dispatcher = context.getRequestDispatcher(Configuration.INDEX_PAGE);
    httpRequest.setAttribute(Configuration.HTTP_INSTANCE_ID, data.instanceID);
    httpRequest.setAttribute(Configuration.HTTP_ID, data.userID);
    httpRequest.setAttribute(Configuration.HTTP_EXTERNAL_ID, data.userExternalID);
    httpRequest.setAttribute(Configuration.HTTP_USER_FIRST_NAME, data.userFirstName);
    httpRequest.setAttribute(Configuration.HTTP_USER_LAST_NAME, data.userLastName);
    httpRequest.setAttribute(Configuration.HTTP_USER_ROLE, data.userRole);
    httpRequest.setAttribute(Configuration.HTTP_USER_LOCALE, data.userLocale);
    
    WeatherClient weatherClient = new OpenWeatherClient(Configuration.getInstance().getOpenweathermapAPIKey());
    httpRequest.setAttribute("temperature", weatherClient.getTemperature("chemnitz", WeatherClient.METRIC));
    
    try {
      dispatcher.forward(httpRequest, httpResponse);
    	
      return null;
    } catch (final Exception e) {
      logger.error("Unable to forward request to jsp: {}", e.getMessage(), e);
      return Response.status(500).build();
    }
  }

  private Response forwardError(
      final HttpServletRequest httpRequest,
      final HttpServletResponse httpResponse,
      final ServletContext context) {
    final RequestDispatcher dispatcher = context.getRequestDispatcher(Configuration.ERROR_PAGE);
    try {
      dispatcher.forward(httpRequest, httpResponse);
      return null;
    } catch (final Exception e) {
      logger.error("Unable to forward request to error jsp: {}", e.getMessage(), e);
      return Response.status(500).build();
    }
  }
}
