package de.kritze.weatherplugin.weatherclient;

/**
 * Defines the methods to obtain a temperature from a weather API.
 */

public interface WeatherClient {
	public static final String METRIC = "metric";
	public static final String IMPERIAL = "imperial";
	
	/**
	 * 
	 * Gets the temperature for a given city name and a defined unit system.
	 * 
	 * @param location  The name of the city for which the temperature should be obtained.
	 * @param units		Defines if the unit should be metric or imperial.
	 */
	public Double getTemperature(String location, String units);
	
	/**
	 * 
	 * Gets the temperature for a given city name in °C.
	 * 
	 * @param location  The name of the city for which the temperature should be obtained.
	 * 
	 */
	public Double getTemperature(String location);
}
