package de.kritze.weatherplugin.weatherclient.openweather;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.filter.LoggingFilter;
import org.json.JSONException;
import org.json.JSONObject;

import de.kritze.weatherplugin.weatherclient.WeatherClient;

public class OpenWeatherClient implements WeatherClient {

	/**
	 * Defines the API version of openweathermap and as a consequence thereof the json structure
	 */
	private static String API_VERSION = "2.5";
	/**
	 * Defines the path for the API call
	 */
	private static String BASE_URL = "http://api.openweathermap.org/data/";
	/**
	 * A key that has to be delivered with every call to the openweathermap API
	 */
	private String apiKey;
	
	/**
	 * Single constructor for OpenWeatherClient.
	 * 
	 * @param apiKey Necessary to call the API. Can be obtained at <a href="http://openweathermap.org/appid#get">openweathermap</a>
	 */
	public OpenWeatherClient(String apiKey) {
		this.apiKey = apiKey;
	}
	
	/**
	  * {@inheritDoc}
	  */
	@Override
	public Double getTemperature(String location, String units) {
		JSONObject weatherdata = getWeatherInformation(location, units);
		try {
			JSONObject main = weatherdata.getJSONObject("main"); //the temperature data is in the "main" block
			return main.getDouble("temp");
		}
		catch(JSONException ex) { //if location is not found or API key is not set
			return null;
		}
		
		
	}

	/**
	  * {@inheritDoc}
	  */
	@Override
	public Double getTemperature(String location) {
		return getTemperature(location,WeatherClient.METRIC);
	}
	
	/**
	 * Calls the openweathermap API and returns the JSONObject with the weather data for the supplied location.
	 * @see JSON structure at <a href="http://openweathermap.org/current#current_JSON">openweathermap</a> 
	 * 
	 * @param location  The name of the city for which the temperature should be obtained.
	 * @param units		Defines if the unit should be metric or imperial.
	 * @return The JSONObject with the weather data from openweathermap
	 */
	private JSONObject getWeatherInformation(String location, String units) {
		
		Client client = ClientBuilder.newClient( new ClientConfig().register( LoggingFilter.class ) );
		
		WebTarget webTarget = client.target(BASE_URL + API_VERSION+ "/weather?q="+location+"&units="+units+"&appid="+apiKey);	
		
		Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.get();
		
		//TODO Error Handling (location not found, unit not found, no temperature data
		// Call the api with the defined path and create a JSONObject with the response data
		JSONObject weatherdata = new JSONObject(response.readEntity(String.class));

		return weatherdata;
	}
}
