package de.kritze.weatherplugin.configuration;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jose4j.keys.HmacKey;

import com.google.common.base.Charsets;

import de.kritze.weatherplugin.PluginModule;

/**
 *  Central collection of configuration data for this plugin.
 *	<p>
 *	Contains configuration values for the endpoint definition and the values of the JWT used for SSO. 
 **/
public class Configuration {
	  
	private static final Logger logger = LogManager.getLogger(PluginModule.class);
	
	/* global values */
	/* java opts or system parameter name */
	public static final String PLUGIN_ID = "eyo_plugin_id";
	public static final String PLUGIN_SECRET = "eyo_plugin_secret";
	public static final String OWM_API_KEY = "owm_api_key";
	
	/* endpoint parameters */
	
	public static final String SSO_ENDPOINT_PATH = "weather";
	public static final String SSO_ENDPOINT_PARAMETER = "eyosso";
	
	/* SSO configuration */ 
	/* eyo.values */
	public static String SSO_INSTANCE_ID= "eyo.i";
	public static String SSO_USER_ID = "eyo.u";
	public static String SSO_USER_EXTERNAL_ID = "eyo.ue";
	public static String SSO_USER_FIRSTNAME = "eyo.ufn";
	public static String SSO_USER_LASTNAME = "eyo.uln";
	public static String SSO_USER_ROLE= "eyo.ur";
	public static String SSO_USER_LANGUAGE = "eyo.ul";
	
	/*Request attributes for forwarding to index.jsp */

	public static String HTTP_INSTANCE_ID = "instanceID";
	public static String HTTP_ID = "id";
	public static String HTTP_EXTERNAL_ID = "externalID";
	public static String HTTP_USER_FIRST_NAME = "firstName";
	public static String HTTP_USER_LAST_NAME = "lastName";
	public static String HTTP_USER_ROLE = "role";
	public static String HTTP_USER_LOCALE = "locale";
	
	/* Urls for forwarding */
	public static String ERROR_PAGE="/errors/500.jsp";
	public static String INDEX_PAGE="/index.jsp";
	
	private static Configuration instance;
	
	/**
	 * Private constructor to suppress the initialization from outside
	 */
	private Configuration() {}
	
	/**
	 * Used to create and return the single instance of this class
	 * 
	 * @return Instance of this class for further use
	 */
	public static Configuration getInstance() {
		if(instance == null) 
			instance = new Configuration();
		return instance;
	}
	
	/**
	 * Returns the API key for openweathermap defined in java opts or as system variable
	 * 
	 * @return The API key
	 */
	public String getOpenweathermapAPIKey() {
		return getProperty(OWM_API_KEY);
	}
	
	/**
	 * Returns the secret defined in java opts or as system variable
	 * 
	 * @return The defined secret
	 */
	public String getSecret() {
		return getProperty(PLUGIN_SECRET);
	}
	
	/**
	 * Get the plugin id defined in java ots or as system variable
	 * 
	 * @return The defined plugin id
	 */
	public String getPluginId() {
		return getProperty(PLUGIN_ID);
	}
	
	/**
	 * Returns the HmacKey for encryption/decryption. 
	 * 
	 * @return HmacKey for encryption/decription 
	 */
	public HmacKey getKey() {
		return new HmacKey(getSecret().getBytes(Charsets.UTF_8));
	}
	
	/**
	 * Returns the property defined as either java opts or system variable by the given name.
	 * 
	 * @param propName	The name of the property to return.
	 * @return 			The value of the property or .not.set if the property can not be found.
	 */
	private String getProperty(final String propName) {

	    // check java opts
	    String propValue = System.getProperty(propName);
	    if (propValue != null) {
	      logger.debug("Found '{}' in java opts.", propName);
	      return propValue;
	    }

	    // check environmental vars
	    propValue = System.getenv(propName);
	    if (propValue != null) {
	      logger.debug("Found '{}' in environmental variables.", propName);
	      return propValue;
	    }

	    // fallback
	    logger.warn("Unable to find '{}'. Please set it in java opts or as "
	        + "environmental variable. Eyo SSO will fail.", propName);
	    return propName + ".not.set";
	  }
}
