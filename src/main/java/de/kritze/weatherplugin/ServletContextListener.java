package de.kritze.weatherplugin;

import java.util.Collections;

import org.glassfish.hk2.api.ServiceLocator;

import com.google.inject.Injector;
import com.google.inject.servlet.GuiceServletContextListener;
import com.squarespace.jersey2.guice.BootstrapUtils;

public class ServletContextListener extends GuiceServletContextListener {

  @Override
  protected Injector getInjector() {
    final ServiceLocator locator = BootstrapUtils.newServiceLocator();
    final Injector injector = BootstrapUtils.newInjector(locator, Collections.singletonList(
        new PluginModule()
    ));
    BootstrapUtils.install(locator);
    return injector;
  }
}
