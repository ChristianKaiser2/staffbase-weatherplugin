<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<html>

<head>
  <c:set var="url">${pageContext.request.requestURL}</c:set>
  <base href="${fn:substring(url, 0, fn:length(url) - fn:length(pageContext.request.requestURI))}${pageContext.request.contextPath}/" />

  <title>Page not found</title>
  <link rel="stylesheet" href="../styles.css"/>
</head>

<body>
  <h1>Page not found</h1>
</body>

</html>
